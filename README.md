# Enzian Shell + DCS

This project contains the Enzian Shell and the DCS application with 2 memory controllers instantiated (configured for 16GB RDIMMs, two channels, 1st and 4th).

To build it, just execute the script `build.sh` (takes 5-6h or `build_fast.sh` to get a worse implemented version in 2-3h).

When it finishes, a directory `build_sample_application` is created. It containts a bitstream `shell_enzian_app_dcs.bit` that can be used to test the Enzian platform out.
