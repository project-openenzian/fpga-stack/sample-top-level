# Path to Vivado binary
VIVADO=/opt/Xilinx/Vivado/2022.1/bin/vivado

# Check if vivado is present
if [ ! -x $VIVADO ]
then
    echo Vivado binary at $VIVADO could not be found!
    exit 1
fi

if [ -e build_static_shell ]
then
    read -e -p "About to delete build_static_shell. Continue? [Y/n]" YN
    [[ $YN != "Y" ]] && exit 1
fi

echo "Building the shell..."
# Create a new clean build directory
rm -rf build_static_shell
mkdir build_static_shell
cd build_static_shell
# Build the shell, optimized
$VIVADO -mode batch -source ../static-shell/build_static_shell_stub.tcl
# Set the environment
export ENZIAN_SHELL_DIR=`pwd`
cd ..

echo "Building the DCS application..."

# Create a new clean build directory
rm -rf build_sample_application
mkdir build_sample_application
cd build_sample_application
# Create the DCS app project
$VIVADO -mode batch -source ../sample-application/create_sample_application_project.tcl
# Build the DCS app
$VIVADO -mode batch -source ../static-shell/build_app.tcl
cd ..

# Done
echo "Done"
